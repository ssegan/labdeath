/**
 * Interface for lists of strings.
 * 
 * @author Michael Siff
 */
public interface StringListInterface
{
    /**
     * Returns true if list is empty, false otherwise.
     * @return whether list is empty
     */
    boolean isEmpty();


    /**
     * Returns number of elements on list.
     * @return number of elements on list
     */
    int size();
    
    /**
     * Returns element at specified position on list.
     * @param index position of element to return
     * @return element at specified position on list
     */
    String get(int index);
    
    /**
     * Replaces whatever was at specified position on list with
     * specified element.
     * @param index position of element to return
     * @param element to put into list
     */
    void set(int index, String element);
    
    /**
     * Appends element onto end of list.
     * @param element the item to append
     */
    void add(String element);
    
    /**
     * Inserts element into list at specified position assuming there
     * is capacity and that index is valid position on list.
     * @param index the position where insertion should take place
     * @param element the item to append
     */
    void add(int index, String element);
    
    /**
     * Returns true if list has specified element.
     * @param element item to be searched for
     * @return whether element is on list
     */
    boolean contains(String element);

    /**
     * Returns position of element in list if present, otherwise returns
     * -1.
     * @param element item to be searched for
     * @return position of element in list or -1 if not found
     */
    int indexOf(String element);

    /**
     * Removes all elements from list.
     */
    void clear();

    /**
     * Removes and returns element from specified position.
     * @param index position of element to remove
     * @return element removed
     */
    String remove(int index);

    /**
     * Returns string representation of list.
     * @return string representation of list
     */
    String toString();
    
}
