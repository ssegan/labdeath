/**
 * Stacks of strings using lists (using resizable arrays).
 * 
 * @author <YOUR NAME(s) HERE>
 */
public class Stack implements StackInterface {
  private StringListInterface _list;
  
  public Stack(){
      _list = new StringList();
    }
    
   public boolean isEmpty() {
        return _list.isEmpty();
    }
    
   public void push(String s) {
       _list.add(s);
    }
   
    public String pop() {
      if (!_list.isEmpty()){
          return _list.remove(_list.size() -1);
        } else {
            throw new RuntimeException("stack underflow");
    }
}
}