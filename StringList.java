/**
 * Lists of strings using resizable arrays.
 * 
 * @author Shreeda & Andre
 */

public class StringList implements StringListInterface {
    private static final int INITIAL_CAPACITY = 32;
    private String[] _array;
    private int _size;
    
    public StringList() {
        _array = new String[INITIAL_CAPACITY];
        _size = 0;
    }
    
    /**
     * Returns true if list is empty, false otherwise.
     * @return whether list is empty
     */
    public boolean isEmpty() {
        return _size == 0;
    }


    /**
     * Returns number of elements on list.
     * @return number of elements on list
     */
    public int size() {
        return _size;
    }

    /**
     * Returns element at specified position on list.
     * @param index position of element to return
     * @return element at specified position on list
     */
    public String get(int index) {
        if (index >= 0 && index < _size) {
            return _array[index];
        } else {
            throw new RuntimeException("invalid list index: " + index);
        }
    }

    
    /**
     * Replaces whatever was at specified position on list with
     * specified element.
     * @param index position of element to return
     * @param element to put into list
     */
    public void set(int index, String element) {
        if (index >= 0 && index < _size) {
            _array[index] = element;
        } else {
            throw new RuntimeException("invalid list index: " + index);
        }
    }
  

        /**
     * Appends element onto end of list assuming there is capacity.
     * @param element the item to append
     */
    public void add(String element) {
        add(_size, element);
    }
    
     /**
     * Inserts element into list at specified position assuming there
     * is capacity and that index is valid position on list.
     * @param index the position where insertion should take place
     * @param element the item to append
     */

    public void add(int index, String element) {
        if (index >= 0 && index <= _size) {
            if (_size == _array.length) {
                String[] newArray = new String[_array.length * 2];
                for (int i = 0; i < _array.length; i++) {
                    newArray[i] = _array[i];
                }
                _array = newArray;
            }
            for (int i = _size; i > index; i--) {
                _array[i] = _array[i-1];
            }
            _array[index] = element;
            _size++;
        } else {
            throw new RuntimeException("invalid list index: " + index);
        }
    }
    
    
    /**
     * Returns true if list has specified element.
     * @param element item to be searched for
     * @return whether element is on list
     */
    public boolean contains(String element) {
        return indexOf(element) != -1;
    }


    /**
     * Returns position of element in list if present, otherwise returns
     * -1.
     * @param element item to be searched for
     * @return position of element in list or -1 if not found
     */
    public int indexOf(String element) {
        int pos = -1;
        int i = 0;
        while (pos == -1 && i < _size) {
            if (_array[i].equals(element)) {
                pos = i;
            } else {
                i++;
            }
        }
        return pos;
    }


    /**
     * Removes all elements from list.
     */
    public void clear() {
        _size = 0;
    }
    

    /**
     * Removes and returns element from specified position.
     * @param index position of element to remove
     * @return element removed
     */
    public String remove(int index) {
        if (index >= 0 && index < _size) {
            String element = _array[index];
            _size--;
            for (int i = index; i < _size; i++) {
                _array[i] = _array[i+1];
            }
            return element;
        } else {
            throw new RuntimeException("invalid list index: " + index);
        }
    }

    /**
     * Returns string representation of list.
     * @return string representation of list
     */
    public String toString() {
        String s = "[";
        if (_size > 0) {
            s += _array[0];
            for (int i = 1; i < _size; i++) {
                s += ", ";
                s += _array[i];
            }
        }
        s += "]";
        return s;
    }
    
}
